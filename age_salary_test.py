from flask import Flask
from flask_restplus import Resource, Api
import joblib
import numpy as np

app = Flask(__name__)
api = Api(app)

model = joblib.load('age_salary.joblib')


@api.route('/<int:age>')
@api.param('age', 'The age to get a salary for')
class Salary(Resource):
    def get(self, age):
        salary = model.predict(np.array([age]).reshape(-1, 1))
        return {'age received': age,
                'expected salary': salary[0].round(2)}

if __name__ == '__main__':
    app.run(debug=True)
