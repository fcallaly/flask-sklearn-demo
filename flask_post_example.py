from flask import Flask, request
from flask_restplus import Resource, Api, fields

app = Flask(__name__)
api = Api(app)

ns = api.namespace('reviews', description='Review operations')

review = api.model('Review', {
    'review': fields.String(required=True, description='The review text')
    })


@ns.route('/')
class Review(Resource):
    def get(self):
        return {'test': 'reviews'}

    @ns.expect(review)
    def post(self):
        return {'request': api.payload['review']}

if __name__ == '__main__':
    app.run(debug=True)
