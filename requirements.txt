boto3==1.9.96
Flask==1.1.1
flask-restplus==0.13.0
joblib==0.14.1
numpy==1.17.4
scikit-learn==0.20
